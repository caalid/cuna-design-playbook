
# CUNA Design Playbook README

**Public Link:** [https://lucid-hamilton-29b7f4.netlify.com](https://lucid-hamilton-29b7f4.netlify.com/)

**Repository Link:** [https://gitlab.com/caalid/cuna-design-playbook](https://gitlab.com/caalid/cuna-design-playbook)

## Documentation

Jekyll :: https://jekyllrb.com/docs/includes/

## Credentials

**Gitlab:** Will send these separately for security.

**Netlify:** Connected via auth with Gitlab

## The CUNA Design Playbook stack

Jekyll :: Jekyll is a lightweight, pre-rendered CMS that transforms collections of markdown files locally into a flat-file website. It uses the Liquid template engine, and it’s blazing fast since the end-user only encounters simple, flat HTML documents in their browser. Plus, it can be hosted almost anywhere – even on an FTP drive. Though, you’ll need SSH access to push updates.

Bootstrap 4 :: Bootstrap is a frontend framework that includes things like layouts, fonts, buttons, colors, and breakpoint support. It makes it quick and easy to set up simple layouts easily and efficiently.

Gitlab :: Gitlab is a free repository host, just like Github. You can clone the project to your local machine using the URL above, or use the Web IDE to edit and commit.

Netlify :: Netlify is a static-site hosting service that can connect to Github or Gitlab repositories. When you make a commit on the repo’s Master branch, either through a git commit or a merge commit, Netlify will automatically pull in those changes from the Master branch to update the site. Right now, the site is on a free tier, which means it exists as a public subdomain on Netlify. We can use the free tier as long as the site stays under a certain threshold for pageviews.

## Updating using Netlify

The __plays_ folder contains markdown files for each play. When you view a markdown file in the Gitlab web viewer, click ‘Web IDE’ on the toolbar to edit online. This way, you don’t have to create a developer environment on your local machine.

When you open the Web IDE, you’ll see a file tree on the left, and your main text window on the right. To create a new Play, you’ll have to copy an existing plays text from the text window, and click the New File icon in the file tree, and then paste in the text as a starting point. There is, at least right now, no duplicate file feature in Gitlab’s Web IDE.

Each Play must have specific YAML data at the top, enclosed in `---`:

* `layout: card` tells Jekyll that this piece of content will use the `card.html` layout, defined in `_layouts`
* `title` is the title of the play
* `permalink` is the absolute URL for the play. It's structured now so that all plays are a top-level URL
* `category` is the design phase that the play should live in
* `description` is a short description that will appear on the card
* `what`, `why`, and `difficulty` are all metadata associated with the play. `difficulty` acceps Easy, Medium, and Hard as values represented by three unfilled or filled dots on the play.
* `gameplan` is an array of Gameplans that the Play can appear in. The Play can appear in multiple Gameplans, as long as the Gameplans are comma separated, and the list of them is wrapped in brackets.

When you write the play content, you can use markdown syntax for things like headers (#, ##, ###, ####, ##### levels), `*` for bulleted lists, `1., 2., etc` for sorted lists, and more.

You can use html to create custom callouts as well:

```
<div class="alert alert-success" role="alert">
  <h4 class="alert-heading">Well done!</h4>
  <p>Your output from this play should be a greater understanding of how your customers or users currently interact with your feature, service, or produce!</p>
</div>
```

## Saving your changes and committing 
Once you're done editing, the blue 'Commit...' button, located towards the bottom of the left file tree, will become blue and clickable. You'll have a chance to review a diff of your changes for each file you've updated, added, or deleted. When you're ready to commit, click the green 'Stage & Commit' button at the bottom left to commit your changes to the Master branch. Remember, committing your changes to the Master branch means that Netlify will re-build the site for you to incorporate those changes.

## Pages
The `_pages` folder contains each Design phase page, and a page for the Gameplans index.

Each page has some metadata as YAML frontmatter, just like Plays:

* `title` should be the page title, most likely the name of the design phase
* `order` is the order of the phases. Note that Gameplan should have it's `order` value set to `99`
* `layout` should be category, so that Jekyll will use `category.html` to display the page, or `gameplan` for the Gameplan index page.
* `category` should be the same as the design phase title, or should be `gameplan` for the Gameplan index page.
* `description` is a brief description of the design phase or page
* `need` is a field that answer's the question for the user 'What need will this phase of the design process help me with?` 

## Gameplans
Gameplans are a collection of plays, and live as markdown files in the `_gameplans` folder. The same workflow for editing and saving Plays applies to Gameplans. Gameplans have associated YAML metadata specific to them as an object type:

* `layout` should be `gameplan`
* `title` should be the title of the Gameplan
* `permalink` should be the permalink for the Gameplan, namespaced after `/gameplans/`
* `category` **must** be `gameplans`
* `description` is a short description of the Gameplan, which will display on the index of Gameplans
* `plan` is the Gameplans title, again. This is redundant but neccessary for some functionality to work, and could be refactored in the future!

The only content that is editable on the markdown file for the Gameplan itself is brief intro text. Remember, the associated Plays for the Gameplan are defined *on the Plays* not the Gameplan.