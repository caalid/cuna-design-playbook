---
layout: card
title: Experience Canvas
permalink: /experience-canvas
category: Discover
description: Lorem ipsum dolor sit amet, consectetur adipiscing elit.

what: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
why: Lorem ipsum dolor sit amet, consectetur adipiscing elit.
difficulty: Medium
gameplan: [New Feature]
---

#### Step One
Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lacinia bibendum nulla sed consectetur. Cras mattis consectetur purus sit amet fermentum.

#### Step Two
Sed posuere consectetur est at lobortis. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Etiam porta sem malesuada magna mollis euismod. Donec id elit non mi porta gravida at eget metus. Integer posuere erat a ante venenatis dapibus posuere velit aliquet. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nullam id dolor id nibh ultricies vehicula ut id elit.

